if (! (Test-Path ".\build\")) {
    New-Item -Path . -Name "build" -ItemType "directory"
} elseif (Test-Path ".\build\package.zip") {
    Remove-Item ".\build\package.zip"
}

$compress = @{
  Path = ".\CHANGELOG.md", ".\icon.png", ".\LICENSE.txt", ".\manifest.json", ".\README.md"
  DestinationPath = ".\build\package.zip"
}
Compress-Archive @compress
