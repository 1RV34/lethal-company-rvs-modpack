# Release Notes

## v4.2.3 (2024-01-10)

- Embed `RVs_Corepack`'s packages directly, using a separate package didn't work.
- Remove `RVs_Corepack`.

## v4.2.2 (2024-01-10)

- Bumped dependency versions.
- Added `RVs_Corepack` to force versions of dependencies for dependencies.

## v4.2.1 (2024-01-09)

- Bumped dependency versions.
- Removed `LCSprayFix`. (Mod was marked deprecated, I assume it's fixed in the base game now.)

## v4.2.0 (2024-01-08)

- Added `Scoopys_Variety_Mod`.

## v4.1.0 (2024-01-08)

- Added `ShootableMouthDogs`.
- Added `NameFix`.
- Added `AccurateStaminaDisplay`.
- Added `Boombox_Sync_Fix`.
- Added `RankFix`.
- Added `JetpackFallFix`.
- Added `FixCentipedeLag`.
- Added `SprintLadderFix`.
- Added `kruumys_FixResolution`.
- Added `ShovelFix`.
- Added `FlashlightFix`.

## v4.0.1 (2024-01-07)

- Added `SlimeTamingFix`.
- Added `DoorFix`.
- Added `DissonanceLagFix`.
- Added `LCSprayFix`.

## v4.0.0 (2024-01-07)

- Swapped out `LethalCompanyDRP` for `LethalRichPresence`.

## v3.2.0 (2024-01-07)

- Added `DiscountAlert`.
- Added `CrossHair`.
- Added `LobbyInviteOnly`.
- Added `ScannableCodes`.
- Added `CompanyBuildingEnhancements`.
- Added `OuijaBoard`.
- Added `Corporate_Restructure`.
- Added `NeedyCats`.
- Added `GamblingMachineAtTheCompany`.
- Added `IntroTweaks`.
- Added `PintoBoy`.
- Added `CozyImprovements`.
- Added `NiceChat`.
- Added `Touchscreen`.
- Added `FairAI`.

## v3.1.0 (2024-01-06)

- `ModelReplacementAPI` no longer conflicts with `More_Suits`.
- Added `BetterStamina`.
- Added `BuyableShotgunShells`.
- Added `NoSellLimit`.

## v3.0.0 (2024-01-05)

- Removed `JnsAudioPack`. (buggy + too loud for some people)

## v2.2.1 (2024-01-05)

- `ModelReplacementAPI` v2.3.2 is incompatible with `More_Suits`, use `ModelReplacementAPI` v2.3.1 until they released a fix.

## v2.2.0 (2024-01-05)

- Added `AdditionalSuits`. (Adds 8 more standard suits to your initial closet at the start of the game.)
- Added `TerminalExtras`. (Extra commands for the terminal.)
- Added `MoreItems`. (Changes the max amount of items that the game saves from 45 to 999.)
- Added `FasterItemDropship`. (Makes the Item Dropship arrive and leave faster.)
- Added `ReservedFlashlightSlot`. (Gives a free, dedicated Flashlight slot on the right side of your screen. Can be toggled with \[F\] to activate the Flashlight at anytime.)
- Added `ReservedWalkieSlot`. (Gives a free, dedicated Walkie slot on the right side of your screen. You can press \[X\] to activate and speak into the Walkie at anytime.)
- Added `ThiccCompanyModelReplacement`. (hehe)
- Added `Hold_Scan_Button`. (This mod allows you to hold the scan button to continuously keep scanning instead of having to spam the button.)
- Added `LethalCompanyDRP`. (Discord Rich Presence for Lethal Company.)
- Added `SuitSaver`. (A plugin for saving your last used suit.)
- Added `JnsAudioPack`. (Replaces a few audio sources with \*high quality\* sounds)
- Added `HealthMetrics`. (Add a simple health counter on your HUD.)
- Added `Terminal_Clock`. (A Terminal Clock.)
- Added `Glow_Steps`. (Adds glowing footsteps where you run! Makes getting out easier.)
- Added `ShipLoot`. (Reliably shows the total value of all scrap in your ship.)
- Added `CoilHeadStare`. (Makes the Coilhead slowly turn and face you even while you are looking directly at it. Reminding you that it only needs you to falter for a single moment.)
- Added `BetterSpec`. (Spectators can now see the clock when dead and other useful info.)

## v2.1.1 (2024-01-05)

- Removed `CompatibilityChecker`. (was causing issues)

## v2.1.0 (2023-12-30)

- Added `LC_Masked_Fix`.
- Added `NameplateTweaks`.
- Added `LCBetterSaves`.
- Added `FrogSkins`.

## v2.0.0 (2023-12-27)

- Swapped out `BiggerLobby` for `MoreCompany`. (in the main menu you'll have a new button in the bottom right to put cosmetics on your character!)
- Added `More_Suits`. (yay more suits!)
- Added `CompatibilityChecker`. (notifies if you're missing required mods when connecting to a host)

## v1.1.0 (2023-12-23)

- Added `LetMeLookDown`. (This mod simply lets you look down all the way. Well, almost all the way.)
- Added `Coroner`. (Rework the Performance Report with new info, including cause of death.)

## v1.0.0 (2023-12-21)

For the homies.
